package com.ihorp.project.service;

import com.ihorp.project.model.Settings;

import java.io.File;
import java.io.IOException;

/**
 *
 */
public class StartStop {

    /**
     *
     */
    private static final File runFile = new File("StopProgram.txt");

    /**
     *
     */
    private final String[] text = {"To stop the program enter 0 in next line or delete this file :)",
            "",
            "How often to check FTP in sec? Standard value 1",
            "",
            "How often to delete old backups in min? Standard value 60.",
            ""};

    public StartStop() {
        if (!runFile.exists()) {
            try {
                runFile.createNewFile();
                io.toLog("File \"StopProgram\" created.", false, new Settings(null));
            } catch (IOException e) {
                io.toLog("Error while creating file \"StopProgram\"", true, new Settings(null));
            }
        }
        io.writeToFile(runFile, text);
    }

    /**
     *
     * @return
     */
    public static boolean isActive() {
        if (io.readFromFile(runFile).get(1) == null) {
            return false;
        } else if (io.readFromFile(runFile).get(1).equals("")) {
            return true;
        }
        return Integer.valueOf(io.readFromFile(runFile).get(1)) != 0;
    }

    /**
     *
     * @return
     */
    public static int ftpCheckDelay() {
        int delay;
        if (io.readFromFile(runFile).get(3).equals("")) {
            return 1000;
        } else if (io.readFromFile(runFile).get(3) == null) {
            io.toLog("Thread " + Thread.currentThread().getName() + " stopped. Cant read file " + runFile.getAbsoluteFile(), true, new Settings(null));
            Thread.currentThread().interrupt();
        }
        delay = Integer.valueOf(io.readFromFile(runFile).get(3));
        return delay * 1000;
    }

    /**
     *
     * @return
     */
    public static int deleteFilesDelay() {
        int delay;
        if (io.readFromFile(runFile).get(5).equals("")) {
            return 60 * 60 * 1000;
        } else if (io.readFromFile(runFile).get(5) == null) {
            io.toLog("Thread " + Thread.currentThread().getName() + " stoped. Cant read file " + runFile.getAbsoluteFile(), true, new Settings(null));
            Thread.currentThread().interrupt();
        }
        delay = Integer.valueOf(io.readFromFile(runFile).get(5));
        return delay * 60 * 1000;
    }
}
