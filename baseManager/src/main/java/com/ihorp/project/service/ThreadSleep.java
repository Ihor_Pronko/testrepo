package com.ihorp.project.service;

/**
 *
 */
public class ThreadSleep {

    public static void sleep(int msec) {
        try {
            Thread.sleep(msec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
