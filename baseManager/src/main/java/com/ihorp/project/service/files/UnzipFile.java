package com.ihorp.project.service.files;

import com.ihorp.project.model.Settings;
import com.ihorp.project.service.io;

import java.io.*;
import java.nio.file.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;


public class UnzipFile {

    public static File unzip(File file, Settings settings) {

        if (!file.exists() || !file.canRead()) {
            System.out.println("File " + file.getName() + " cannot be read");
            return null;
        }

        File filePE;
        try {
            ZipFile zip = new ZipFile(file);
            Enumeration entries = zip.entries();

            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();

                if (entry.isDirectory()) {
                    new File(file.getParent(), entry.getName()).mkdirs();
                } else {
                    write(zip.getInputStream(entry),
                            new BufferedOutputStream(new FileOutputStream(
                                    new File(file.getParent(), entry.getName()))));
                }
            }
            zip.close();
            Files.delete(file.toPath());
            io.toLog("File " + file.getAbsoluteFile() + " unziped", false, settings);

            filePE = new File(file.getParent(), file.getName().replaceAll(".zip$", ""));

        } catch (ZipException p) {
            io.toLog("File " + file + " is empty!", true, settings);
            try {
                Files.move(file.toPath(), Paths.get(settings.getBadFiles() + File.separator + file.getName()), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                io.toLog("Error while move bad file" + file.getAbsoluteFile() + " to " + settings.getBadFiles(),
                        true, settings);
            }
            return null;
        } catch (IOException e) {
            io.toLog("IOException while unzip file" + file.getName(), true, settings);
            return null;
        }
        return filePE;
    }

    private static void write(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int len;
        while ((len = in.read(buffer)) >= 0)
            out.write(buffer, 0, len);
        out.close();
        in.close();
    }
}
