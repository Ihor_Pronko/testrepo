package com.ihorp.project.service.files;

import com.ihorp.project.model.Settings;
import com.ihorp.project.model.files.BackupFileImpl;
import com.ihorp.project.service.StartStop;
import com.ihorp.project.service.ThreadSleep;
import com.ihorp.project.service.io;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 *
 */
public class DeleteOldBackupsFromActual extends Thread {

    public DeleteOldBackupsFromActual() {
        super("DeleteOldBackupsFromActual");
    }

    @Override
    public void run() {
        io.toLog("Thread " + Thread.currentThread().getName() + " started", false, new Settings(null));
        while (true) {
            File[] files = new File(Paths.get("").toAbsolutePath().toString()).listFiles();
            Arrays.stream(files)
                    .filter(o -> o.getName().matches("^Settings.*\\.txt"))
                    .filter(file -> new Settings(file).isActive())
                    .map(Settings::new)
                    .forEach(o -> BackupFileImpl.clean(o));
            ThreadSleep.sleep(StartStop.deleteFilesDelay());
        }
    }
}
