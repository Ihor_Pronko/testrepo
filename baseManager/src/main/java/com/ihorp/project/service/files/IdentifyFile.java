package com.ihorp.project.service.files;

import com.ihorp.project.model.*;
import com.ihorp.project.model.files.*;

import java.io.File;

/**
 *
 */
public class IdentifyFile {

    public static FilePE identifyFile(File file, Settings settings) {

        if (file == null) {
            return null;
        }

        if (file.getName().matches("^BackUp_1{1,2}_[\\d]{6}_[\\d]{6}_[\\d]{6}.bak$")) {
            return new BackupFileImpl(file);
        } else if (file.getName().matches("^[\\d]{9}.(002|003|004|502)$")) {
            return new BuhFileImpl(file);
        } else if (file.getName().matches("^[\\d]{9}.*\\.(100|101)$")) {
            return new BaseFileImpl(file);
        } else if (file.getName().matches("^up[\\d]{4}_[\\d]{6}.[\\d]{3}$")) {
            return new UpdateLogFileImpl(file);
        } else if (file.getName().matches(".*\\.zip$")) {
            return identifyFile(UnzipFile.unzip(file, settings), settings);
        } else if (!file.getName().matches(".*\\.tmp$")) {
            return new NonExpectedFileImpl(file);
        }
        return null;
    }
}
