package com.ihorp.project.service;

import com.ihorp.project.model.Settings;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

/**
 * Util class.
 */
public class io {

    /**
     * @param file
     * @return
     */
    public static List<String> readFromFile(File file) {

        List<String> listLines;
        try {
            listLines = Files.readAllLines(Paths.get(file.toURI()), StandardCharsets.UTF_8);
        } catch (NoSuchFileException w) {
            w.printStackTrace();
            toLog("File not found " + file.getName(), true, new Settings(null));
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            toLog("Error while read file " + file.getName(), true, new Settings(null));
            return null;
        }
        return listLines;
    }

    /**
     * @param file
     * @param text
     */
    public static void writeToFile(File file, String[] text) {
        List<String> lines = Arrays.asList(text);
        try {
            Files.write(Paths.get(file.toURI()), lines, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param
     * @param text
     */
    public static void toLog(String text, boolean isError, Settings settings) {

        if (settings.getCompanyName() == null) {
            settings.setCompanyName("BaseManager");
        }

        File logFile = new File("Logs"
                + File.separator + settings.getCompanyName()
                + File.separator + "Log " + LocalDate.now() + ".txt");

        if (!new File(logFile.getParent()).exists()) {
            new File(logFile.getParent()).mkdirs();
        }


        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Error while creating log file");
            }
        }

        String message = "INFO " + " " + LocalTime.now().toString() + ": " + text + "\n";

        if (isError) {
            message = "ERROR " + " " + LocalTime.now().toString() + ": " + text + "\n";
        }

        try {
            Files.write(logFile.toPath(), message.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error while writing to log");
        }
    }
}
