package com.ihorp.project.service.files;

import com.ihorp.project.model.Settings;
import com.ihorp.project.service.StartStop;
import com.ihorp.project.service.ThreadSleep;
import com.ihorp.project.service.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 *
 */
public class MoveFilesToStorage extends Thread{


    public MoveFilesToStorage() {
        super("MoveFilesToStorage");
    }

    @Override
    public void run() {

        io.toLog("Thread " + Thread.currentThread().getName() + " started", false, new Settings(null));
        while(true) {
            File[] files = new File(Paths.get("").toAbsolutePath().toString()).listFiles();
            Arrays.stream(files)
                    .filter(o -> o.getName().matches("^Settings.*\\.txt"))
                    .filter(file -> new Settings(file).isActive())
                    .map(Settings::new)
                    .forEach(o -> {
                        try {
                            Files.walk(Paths.get(o.getFtpPath()))
                                    .filter(Files::isRegularFile)
                                    .map(Path::toFile)
                                    .filter(y -> y.getAbsolutePath().matches(".*[/\\\\]In[/\\\\].*"))
                                    .filter(y2 -> !y2.getName().matches(".*\\.tmp$"))
                                    .map(y3 -> IdentifyFile.identifyFile(y3, o))
                                    .filter(y4 -> y4 != null)
                                    .forEach(e -> e.replace(o));
                        } catch (NoSuchFileException k) {
                            k.printStackTrace();
                            io.toLog("NoSuchFile while MoveFilesToStorage (Не правильно указана папка FTP либо название компании.\n" +
                                    "Название компани должно в точности совпадать с названием ее папки на FTP сервере", true, o);
                        } catch (IOException e) {
                            io.toLog("IOException while Files.walk in " + o.getFtpPath(), true, o);
                        }
                    });
            ThreadSleep.sleep(StartStop.ftpCheckDelay());
        }
    }
}
