package com.ihorp.project.main;

import com.ihorp.project.model.Settings;
import com.ihorp.project.service.*;
import com.ihorp.project.service.files.DeleteOldBackupsFromActual;
import com.ihorp.project.service.files.MoveFilesToStorage;

public class Main {

    public static void main(String[] args) {

        io.toLog("Program start", false, new Settings(null));

        //Creating a file with example of settings.
        boolean run = !Settings.createExampleSettings();

        //Create a file with Program settings
        new StartStop();

        //Start thread moveFiles.
        MoveFilesToStorage moveFiles = new MoveFilesToStorage();
        moveFiles.setDaemon(true);
        moveFiles.start();

        //Start thread delete deleteOldBackups.
        DeleteOldBackupsFromActual deleteOldBackups = new DeleteOldBackupsFromActual();
        deleteOldBackups.setDaemon(true);
        deleteOldBackups.start();

        // Stop program.
        while (run) {
            ThreadSleep.sleep(1000);
            run = StartStop.isActive();
        }
        io.toLog("Program stopped", false, new Settings(null));
    }
}



