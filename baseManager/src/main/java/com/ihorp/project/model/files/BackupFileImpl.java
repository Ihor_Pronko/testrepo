package com.ihorp.project.model.files;

import com.ihorp.project.model.FilePE;
import com.ihorp.project.model.Settings;
import com.ihorp.project.service.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

/**
 * Class of Backup's program PawnExpert.
 */
public class BackupFileImpl implements FilePE {

    private int branchCode;

    private LocalDate dateOfBackup;

    private LocalTime timeOfBackup;

    private String storageFileName;

    private String storageArchivePath;

    private File file;

    private boolean isBackup;

    public BackupFileImpl(File file) {
        if (isBackup(file)) {
            this.dateOfBackup = findDate(file);
            this.timeOfBackup = findTime(file);
            this.branchCode = findBranchCode(file);
            this.file = file;
            storageFileName = branchCode + " "
                    + dateOfBackup.toString() + " "
                    + timeOfBackup.getHour() + "-" + timeOfBackup.getMinute() + "-" + timeOfBackup.getSecond()
                    + ".zip";
            storageArchivePath = dateOfBackup.getYear() + File.separator
                    + dateOfBackup.getMonthValue() + "_" + dateOfBackup.getMonth().toString() + File.separator
                    + branchCode;

            isBackup = true;
        } else {
            isBackup = false;
        }
    }

    private static boolean isBackup(File file) {
        return file.getName().matches("^\\d+\\s\\d{4}.*\\.zip") ||
                file.getName().matches("BackUp_1{1,2}_[\\d]{6}_[\\d]{6}_[\\d]{6}.bak");
    }

    /**
     * @param file
     * @return
     */
    public static LocalDate findDate(File file) {
        if (file == null) {
            return null;
        }

        if (file.getName().matches("^\\d+\\s\\d{4}.*\\.zip")) {
            String date = file.getName().replaceAll("^[^\\s]+\\s", "");
            return LocalDate.of(Integer.valueOf(date.substring(0, 4)),
                    Integer.valueOf(date.substring(5, 7)),
                    Integer.valueOf(date.substring(8, 10)));
        }

        int hasNoShop = 0;
        if (file.getName().length() == 33) {
            hasNoShop = 1;
        }
        return LocalDate.of(Integer.valueOf("20" + file.getName().substring(17 - hasNoShop, 19 - hasNoShop)),
                Integer.valueOf(file.getName().substring(19 - hasNoShop, 21 - hasNoShop)),
                Integer.valueOf(file.getName().substring(21 - hasNoShop, 23 - hasNoShop)));
    }

    /**
     * @param file
     * @return
     */
    public static LocalTime findTime(File file) {
        if (file == null) {
            return null;
        }

        if (file.getName().matches("^\\d+\\s\\d{4}.*")) {
            String time = file.getName().replaceAll(".+\\s", "");
            int hour = Integer.valueOf(time.replaceAll("-.*", ""));
            int min = Integer.valueOf(time.replaceAll("^\\d+-", "").replaceAll("-\\d+.zip$", ""));
            int sec = Integer.valueOf(time.replaceAll(".*-|.zip", ""));
            return LocalTime.of(hour, min, sec);
        }

        int hasNoShop = 0;
        if (file.getName().length() == 33) {
            hasNoShop = 1;
        }
        return LocalTime.of(Integer.valueOf(file.getName().substring(24 - hasNoShop, 26 - hasNoShop)),
                Integer.valueOf(file.getName().substring(26 - hasNoShop, 28 - hasNoShop)),
                Integer.valueOf(file.getName().substring(28 - hasNoShop, 30 - hasNoShop)));
    }

    /**
     * @param file
     * @return
     */
    public static int findBranchCode(File file) {
        if (file == null) {
            return 0;
        }

        if (file.getName().matches("\\d+\\s\\d{4}.*")) {
            return Integer.valueOf(file.getName().replaceAll("\\s.*", ""));
        }

        int hasNoShop = 0;
        if (file.getName().length() == 33) {
            hasNoShop = 1;
        }
        return Integer.valueOf(file.getName().substring(13 - hasNoShop, 16 - hasNoShop));
    }

    /**
     * @param settings
     * @return
     */
    @Override
    public boolean replace(Settings settings) {
        String absoluteBackupFileName = settings.getStorageBackupPath()
                + File.separator + branchCode
                + File.separator + storageFileName;

        File pathBackUp = new File(settings.getStorageBackupPath() + File.separator + branchCode);
        if (!pathBackUp.exists()) {
            pathBackUp.mkdirs();
        }

        moveToArchive(settings);

        try {
            Files.copy(Paths.get(file.toURI()),
                    Paths.get(absoluteBackupFileName),
                    StandardCopyOption.REPLACE_EXISTING);
            io.toLog("File " + file.getAbsoluteFile() + " had been copied to " + absoluteBackupFileName, false, settings);
            Files.delete(Paths.get(file.toURI()));
            io.toLog("File " + file.getAbsoluteFile() + " had been deleted ", false, settings);
        } catch (IOException e) {
            io.toLog("Error while copy file " + file.getAbsoluteFile() + " to " + absoluteBackupFileName + " or while deleting this file",
                    true,
                    settings);
            return false;
        }

        return true;
    }

    /**
     * @param
     * @return
     */
    private void moveToArchive(Settings settings) {
        new File(settings.getStorageArchivePath() + File.separator + this.getStorageArchive()).mkdirs();

        try {
            if (Arrays.stream(new File(settings.getStorageArchivePath() + File.separator
                    + this.getStorageArchive()).listFiles())
                    .map(BackupFileImpl::new)
                    .filter(BackupFileImpl::isBackup)
                    .filter(o -> o.getDateOfBackup().getDayOfMonth() >= settings.getMinDateToArchive())
                    .noneMatch(o -> o.dateOfBackup.getDayOfMonth() <= this.getDateOfBackup().getDayOfMonth())
                    && this.dateOfBackup.getDayOfMonth() >= settings.getMinDateToArchive() ) {
                Files.copy(this.getFile().toPath(),
                        new File(settings.getStorageArchivePath() + File.separator
                                + this.getStorageArchive() + File.separator
                                + this.storageFileName).toPath(),
                        StandardCopyOption.REPLACE_EXISTING);

                io.toLog("File " + this.getFile().getAbsoluteFile() + " had been copied to "
                                + new File(settings.getStorageArchivePath() + File.separator
                                + this.getStorageArchive() + File.separator + this.storageFileName),
                        false,
                        settings);
            }
        } catch (IOException e) {
            io.toLog("IOException while copy " + this.getFile().getName() + " to "
                            + new File(settings.getStorageArchivePath() + File.separator
                            + this.getStorageArchive() + File.separator + this.storageFileName),
                    true,
                    settings);
        }
    }

    /**
     *
     */
    public static void clean(Settings settings) {
        File file = new File(settings.getStorageBackupPath());
        Arrays.stream(file.listFiles())
                .filter(File::isDirectory)
                .forEach(o -> Arrays.stream(o.listFiles())
                        .sorted(Comparator.comparing((e) -> new BackupFileImpl(e).getDateOfBackup(), Comparator.reverseOrder()))
                        .skip(settings.getHowManyBackupsToKeep())
                        .map(File::toPath)
                        .forEach(y -> {
                            try {
                                Files.delete(y);
                                io.toLog("File " + y.toFile().getName() + " deleted. " + "Number of days to store the file: " + settings.getHowManyBackupsToKeep(),
                                        false,
                                        settings);
                            } catch (IOException e) {
                                io.toLog("IOException while deleting file" + y.toFile().getName(), true, settings);
                            }
                        }));
    }

    public int getBranchCode() {
        return branchCode;
    }

    public LocalDate getDateOfBackup() {
        return dateOfBackup;
    }

    public LocalTime getTimeOfBackup() {
        return timeOfBackup;
    }

    public File getFile() {
        return file;
    }

    public String getStorageFileName() {
        return storageFileName;
    }

    public String getStorageArchive() {
        return storageArchivePath;
    }

    public boolean isBackup() {
        return isBackup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BackupFileImpl that = (BackupFileImpl) o;

        if (branchCode != that.branchCode) return false;
        if (isBackup != that.isBackup) return false;
        if (dateOfBackup != null ? !dateOfBackup.equals(that.dateOfBackup) : that.dateOfBackup != null) return false;
        if (timeOfBackup != null ? !timeOfBackup.equals(that.timeOfBackup) : that.timeOfBackup != null) return false;
        if (storageFileName != null ? !storageFileName.equals(that.storageFileName) : that.storageFileName != null)
            return false;
        if (storageArchivePath != null ? !storageArchivePath.equals(that.storageArchivePath) : that.storageArchivePath != null)
            return false;
        return file != null ? file.equals(that.file) : that.file == null;
    }

    @Override
    public int hashCode() {
        int result = branchCode;
        result = 31 * result + (dateOfBackup != null ? dateOfBackup.hashCode() : 0);
        result = 31 * result + (timeOfBackup != null ? timeOfBackup.hashCode() : 0);
        result = 31 * result + (storageFileName != null ? storageFileName.hashCode() : 0);
        result = 31 * result + (storageArchivePath != null ? storageArchivePath.hashCode() : 0);
        result = 31 * result + (file != null ? file.hashCode() : 0);
        result = 31 * result + (isBackup ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BackupFileImpl{" +
                "file=" + file.getName() +
                '}';
    }
}
