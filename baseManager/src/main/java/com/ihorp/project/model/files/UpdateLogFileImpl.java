package com.ihorp.project.model.files;

import com.ihorp.project.model.FilePE;
import com.ihorp.project.model.Settings;
import com.ihorp.project.service.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;

/**
 *
 */
public class UpdateLogFileImpl implements FilePE {

    private int branchCode;

    LocalDate dateFile;

    File file;

    public UpdateLogFileImpl(File file) {
        branchCode = findBranchCode(file);
        dateFile = findDate(file);
        this.file = file;
    }

    @Override
    public boolean replace(Settings settings) {
        File absoluteFileName = new File(settings.getUpdateLogPath()
                + File.separator + "Update " + branchCode + " " + dateFile + ".txt");
        if (!new File(settings.getUpdateLogPath()).exists()) {
            new File(settings.getUpdateLogPath()).mkdirs();
        }

        try {
            Files.copy(file.toPath(),
                    absoluteFileName.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
            io.toLog("File " + file.getAbsoluteFile() + " had been copied to " + absoluteFileName, false, settings);
            Files.delete(file.toPath());
            io.toLog("File " + file.getAbsoluteFile() + " had been deleted", false, settings);
            file = absoluteFileName;
        } catch (IOException e) {
            io.toLog("IOException while copying file " + file.getAbsoluteFile()
                    + " to " + absoluteFileName
                    + " or while deleting this file", true, settings);
            return false;
        }
        return true;
    }

    private int findBranchCode(File file) {
        return Integer.valueOf(file.getName().replaceAll(".*\\.", ""));
    }

    private LocalDate findDate(File file) {
        String date = file.getName().replaceAll(".*_", "");
        return LocalDate.of(Integer.valueOf("20" + date.substring(0, 2)),
                Integer.valueOf(date.substring(2, 4)),
                Integer.valueOf(date.substring(4, 6)));
    }

    public int getBranchCode() {
        return branchCode;
    }

    public LocalDate getDateFile() {
        return dateFile;
    }

    public File getFile() {
        return file;
    }
}
