package com.ihorp.project.model;

import com.ihorp.project.service.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 *
 */
public class Settings {

    private final int activeLine = 1;
    private boolean active;

    private final int companyNameLine = 4;
    private String companyName;

    private final int ftpPathLine = 7;
    private String ftpPath;

    private final int storagePathLine = 10;
    private String storagePath;

    private final int howManyBackupsToKeepLine = 13;
    private int howManyBackupsToKeep;

    private final int minDateToArchiveLine = 16;
    private int minDateToArchive;

    private String storageBackupPath;

    private String storageArchivePath;

    private String buhPath;

    private String basePath;

    private String updateLogPath;

    private String ufoFilesPath;

    private String badFiles;

    private File file;

    public Settings(File file) {
        if (file != null
                && io.readFromFile(file) != null
                && io.readFromFile(file).size() != 0
                && !io.readFromFile(file).get(activeLine).equals("0")) {
            companyName = io.readFromFile(file).get(companyNameLine);
            ftpPath = io.readFromFile(file).get(ftpPathLine) + File.separator + companyName;
            storagePath = io.readFromFile(file).get(storagePathLine);
            howManyBackupsToKeep = io.readFromFile(file).get(howManyBackupsToKeepLine).equals("") ? 7 : Integer.valueOf(io.readFromFile(file).get(howManyBackupsToKeepLine));
            minDateToArchive = io.readFromFile(file).get(minDateToArchiveLine).equals("") ? 0 : Integer.valueOf(io.readFromFile(file).get(minDateToArchiveLine));
            storageBackupPath = storagePath + File.separator + companyName + File.separator + "BackupPE_ACTUAL";
            storageArchivePath = storagePath + File.separator + companyName + File.separator + "BackupPE_ARCHIVE";
            buhPath = storagePath + File.separator + companyName + File.separator + "BUH_" + companyName;
            basePath = storagePath + File.separator + companyName + File.separator + "BaseExpert_DirIn";
            updateLogPath = storagePath + File.separator + companyName + File.separator + "UpdatesPE_Logs";
            ufoFilesPath = storagePath + File.separator + companyName + File.separator + "Non_Recognized_Files";
            badFiles = storagePath + File.separator + companyName + File.separator + "Bad_Files";
            this.file = file;

            new File(storageBackupPath).mkdirs();
            new File(storageArchivePath).mkdirs();
            new File(buhPath).mkdirs();
            new File(basePath).mkdirs();
            new File(updateLogPath).mkdirs();
            new File(ufoFilesPath).mkdirs();
            new File(badFiles).mkdirs();
            if (!new File(ftpPath).exists()) {
                io.toLog(ftpPath + File.separator + companyName + " FALSE. Check the company name and ftp path!", true, new Settings(null));
            }
            active = !io.readFromFile(file).get(activeLine).equals("0") && isFillAllFields();
        }
    }

    public boolean isFillAllFields() {
        if (companyName.equals("")
                || ftpPath.equals("")
                || storagePath.equals("")) {
            return false;
        }
        return true;
    }

    public static boolean createExampleSettings() {
        String[] exampleText = {"Is settings active? 0 - No.",
                "0",
                "---------------",
                "Company Name:",
                "",
                "---------------",
                "FTP Path current company:",
                "",
                "---------------",
                "Storage path:",
                "",
                "---------------",
                "How many backups to keep:",
                "",
                "---------------",
                "The minimum date to move to the archive:",
                "",
                "---------------",
                "Важно! Сохраняйте последовательность строк в данном файле. ",
                "",
                "Строка №2* - включени или выключение настроек. 0 - не активны.",
                "Внимание! Что бы настройки были активны, все обязательные строки должны быть заполнены!",
                "",
                "Строка №5* - Название предприятия. Должно совпадать с названием папки на ФТП сервере",
                "",
                "Строка №8* - полный путь к директории ftp сервера до разделения на предприятия.",
                "",
                "Строка №11* - полный путь к директории где будет находится структура папок по каждому предприятию. Структура создается автоматически.",
                "",
                "Строка №14 - количество бекапов на хранении по каждому ЛО. Не рекоммендуется ставить меньше 2х. По умолчанию 7",
                "",
                "Строка №17 - какую дату отправлять на хранение в архив. Выставляется минимальная дата, например если",
                "есть 2 бекапа за 1е число и за 4е, а минимальная дата стоит \"2\" то на хранение уйдет бекап за 4е числ",
                "По умолчанию 0",
                "",
                "* Строки, обязательные к заполнению"};

        File[] files = new File(Paths.get("").toAbsolutePath().toString()).listFiles();

        if (!Arrays.stream(files)
                .anyMatch(o -> o.getName().matches("^Settings.*\\.txt"))) {
            File file = new File("Settings_Example_MUST_BE_UTF8.txt");
            try {
                file.createNewFile();
                io.writeToFile(file, exampleText);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getFtpPath() {
        return ftpPath;
    }

    public String getStorageBackupPath() {
        return storageBackupPath;
    }

    public String getStorageArchivePath() {
        return storageArchivePath;
    }

    public int getHowManyBackupsToKeep() {
        return howManyBackupsToKeep;
    }

    public int getMinDateToArchive() {
        return minDateToArchive;
    }

    public String getBuhPath() {
        return buhPath;
    }

    public String getBasePath() {
        return basePath;
    }

    public String getUpdateLogPath() {
        return updateLogPath;
    }

    public String getUfoFilesPath() {
        return ufoFilesPath;
    }

    public File getFile() {
        return file;
    }

    public String getBadFiles() {
        return badFiles;
    }

    public boolean isActive() {
        return active;
    }
}
