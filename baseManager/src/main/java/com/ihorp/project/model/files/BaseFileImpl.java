package com.ihorp.project.model.files;

import com.ihorp.project.model.FilePE;
import com.ihorp.project.model.Settings;
import com.ihorp.project.service.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;

/**
 *
 */
public class BaseFileImpl implements FilePE {

    private int branchCode;

    private LocalDate date;

    private File file;

    public BaseFileImpl(File file) {
        branchCode = findBranchCode(file);
        date = findDate(file);
        this.file = file;
    }

    private int findBranchCode(File file) {
        return Integer.valueOf(file.getName().substring(0, 3));
    }

    private LocalDate findDate(File file) {
        String date = file.getName().substring(3, 9);
        return LocalDate.of(Integer.valueOf("20" + date.substring(0, 2)),
                Integer.valueOf(date.substring(2, 4)),
                Integer.valueOf(date.substring(4, 6)));
    }

    @Override
    public boolean replace(Settings settings) {
        File absoluteFileName = new File(settings.getBasePath() + File.separator + file.getName());
        if (!new File(settings.getBasePath()).exists()) {
            new File(settings.getBasePath()).mkdirs();
        }
        try {
            Files.copy(file.toPath(),
                    Paths.get(settings.getBasePath() + File.separator + file.getName()),
                    StandardCopyOption.REPLACE_EXISTING);
            io.toLog("File " + file.getAbsoluteFile() + " had been copied to "
                    + settings.getBasePath() + File.separator + file.getName(), false, settings);
            Files.delete(file.toPath());
            io.toLog("File " + file.getAbsoluteFile() + " had been deleted", false, settings);
            file = absoluteFileName;
        } catch (IOException e) {
            io.toLog("IOException while copying file " + file.getAbsoluteFile()
                    + " to " + settings.getBasePath() + File.separator + file.getName()
                    + " or while deleting this file", true, settings);
            return false;
        }
        return true;
    }

    /**
     * Checks is any file are missing
     *
     * @param settings
     */
    public void checkBaseFiles(Settings settings) {
        //to do
    }

    public int getBranchCode() {
        return branchCode;
    }

    public LocalDate getDate() {
        return date;
    }
}
