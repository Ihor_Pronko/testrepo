package com.ihorp.project.model.files;

import com.ihorp.project.model.FilePE;
import com.ihorp.project.model.Settings;
import com.ihorp.project.service.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

/**
 *
 */
public class NonExpectedFileImpl implements FilePE {

    private File file;

    public NonExpectedFileImpl(File file) {
        this.file = file;
    }

    @Override
    public boolean replace(Settings settings) {
        File absoluteFileName = new File(settings.getUfoFilesPath() + File.separator + file.getName());
        if (!new File(settings.getUfoFilesPath()).exists()) {
            new File(settings.getUfoFilesPath()).mkdirs();
        }
        try {
            Files.copy(file.toPath(),
                    absoluteFileName.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
            io.toLog("File " + file.getAbsoluteFile() + " had been copied to " + absoluteFileName, false, settings);
            Files.delete(file.toPath());
            io.toLog("File " + file.getAbsoluteFile() + " had been deleted", false, settings);
            file = absoluteFileName;
        } catch (IOException e) {
            io.toLog("IOException while copying file " + file.getAbsoluteFile()
                    + " to " + absoluteFileName
                    + " or while deleting this file", true, settings);
            return false;
        }
        return true;
    }
}
