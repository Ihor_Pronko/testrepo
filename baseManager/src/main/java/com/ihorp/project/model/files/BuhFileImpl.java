package com.ihorp.project.model.files;

import com.ihorp.project.model.FilePE;
import com.ihorp.project.model.Settings;
import com.ihorp.project.service.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;

/**
 *
 */
public class BuhFileImpl implements FilePE {

    private int branchCode;

    LocalDate dateFile;

    File file;

    public BuhFileImpl(File file) {
        branchCode = findBranchCode(file);
        dateFile = findDate(file);
        this.file = file;
    }

    private int findBranchCode(File file) {
        return Integer.valueOf(file.getName().substring(0, 3));
    }

    private LocalDate findDate(File file) {
        String date = file.getName().substring(3, 9);
        return LocalDate.of(Integer.valueOf("20" + date.substring(0, 2)),
                Integer.valueOf(date.substring(2, 4)),
                Integer.valueOf(date.substring(4, 6)));
    }


    @Override
    public boolean replace(Settings settings) {
        File absoluteFileName = new File(settings.getBuhPath() + File.separator + file.getName());
        if (!new File(settings.getBuhPath()).exists()) {
            new File(settings.getBuhPath()).mkdirs();
        }
        try {
            Files.copy(file.toPath(),
                    absoluteFileName.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
            io.toLog("File " + file.getAbsoluteFile() + " had been copied to " + absoluteFileName, false, settings);
            Files.delete(file.toPath());
            io.toLog("File " + file.getAbsoluteFile() + " had been deleted", false, settings);
            file = absoluteFileName;
        } catch (IOException e) {
            io.toLog("IOException while copying file " + file.getAbsoluteFile()
                    + " to " + absoluteFileName
                    + " or while deleting this file", true, settings);
            return false;
        }
        return true;
    }

    /**
     *
     */
    public void clean() {
        //to do
    }

    /**
     *
     */
    public void checkBuhFiles() {
        //to do
    }

}
