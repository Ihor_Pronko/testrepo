package tests;

import com.ihorp.project.model.Settings;
import com.ihorp.project.model.files.BaseFileImpl;
import com.ihorp.project.model.files.UpdateLogFileImpl;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;


public class test {

    @Test
    public void test() {
        File path = new File("/Users/ihor/Desktop/music/vk/test");
        for (File file :
                path.listFiles()) {
            System.out.println(file.getName());
            System.out.println(file.getPath().toString());
        }
    }

    @Test
    public void testOne() throws IOException {
        System.out.println(Paths.get("").toAbsolutePath().toString() + "/Settings.txt");
        Files.readAllLines(Paths.get(new File("/Users/ihor/IdeaProjects/testrepo/manageBase/Settings1.txt").toURI()), UTF_8).stream().forEach(System.out::println);
    }

    @Test
    public void testFour() {
        List<Integer> integers = new ArrayList<>();
        integers.stream().forEach(System.out::println);
    }

    @Test
    public void testDate() {
        LocalDate date = LocalDate.of(2017, 06, 06);
        System.out.println(date.toString());
    }

    @Test
    public void testCurrentUri() {
        System.out.println(Paths.get(new File("").getAbsolutePath()));
    }

    @Test
    public void testNow() {
        System.out.println(LocalDate.now());
    }

    @Test
    public void testName() {
        int branchCode = 301;
        LocalDate dateOfBackup = LocalDate.of(2017, 06, 06);
        String storageBackupFileName = branchCode + " " + dateOfBackup.toString() + ".zip";
        System.out.println(storageBackupFileName);
    }


    @Test
    public void listOfFiles() throws IOException {
        Files.walk(Paths.get("/Users/ihor/Desktop/java/Код/BaseExpert_project/test_files/ftp"))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .collect(Collectors.toList())
                .stream().map(o -> o.getName())
                .forEach(System.out::println);
    }

    @Test
    public void testCopy() throws IOException {
        String string = "2017-06-06";
        System.out.println(LocalDate.of(Integer.valueOf(string.substring(0, 4)),
                Integer.valueOf(string.substring(5, 7)),
                Integer.valueOf(string.substring(8, 10))).toString());
    }

    @Test
    public void testPath() {
        System.out.println(new File("2017/June/1").getAbsolutePath());
    }

    @Test
    public void test12313() {
        File file = new File("/Users/ihor/Desktop/java/Код/BaseExpert_project/test_files/ftp/out");
        boolean isEmpty = false;
        if (file.list() == null || file.list().length == 0) {
            isEmpty = true;
        }
        System.out.println(isEmpty);
    }

    @Test
    public void test235() {
        try {
            Files.walk(Paths.get("/Users/ihor/Desktop/java/Код/BaseExpert_project/test_files/backup_archive/Test_Company/2017/JUNE/4"))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList())
                    .stream()
                    //.filter(o -> o.getName().matches(".*\\.zip"))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testD() {
        File file = new File("/Users/ihor/Desktop/java/Код/BaseExpert_project/test_files/backup_archive/Test_Company/PE_BackUps/19/19 2017-06-20.zip");
        System.out.println(file.getName().matches("\\d+\\s\\d{4}.*"));
    }

    @Test
    public void testF() {
        File fileOne = new File(Paths.get("").toAbsolutePath().toString());
        File fileTwo = new File("/");
        System.out.println(fileOne.toString() + "\n" + fileTwo.toString());
    }

    @Test
    public void tesdK() {
        Arrays.stream(new File(Paths.get("").toAbsolutePath().toString()).listFiles())
                .filter(o -> o.getName().matches("^Settings.*\\.txt"))
                .map(Settings::new)
                .forEach(o -> {
                    try {
                        Files.walk(Paths.get(o.getFtpPath()))
                                .filter(Files::isRegularFile)
                                .map(Path::toFile)
                                .collect(Collectors.toList())
                                .forEach(e -> System.out.println(e.getName()));
                        //.forEach(e -> IdentifyFile.identifyFile(e).replace(o));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    @Test
    public void teste() {
        File file = new File("001170223.100");
        BaseFileImpl base = new BaseFileImpl(file);
        System.out.println(base.getDate());
    }

    @Test
    public void testeere() {
        File file = new File("up0021_170111.932");
        UpdateLogFileImpl update = new UpdateLogFileImpl(file);
        System.out.println(update.getBranchCode());
    }

    @Test
    public void test234234() {
        File[] files = new File("/Users/ihor/IdeaProjects/testrepo").listFiles();

        for (File file : files) {
            if (file.getName().matches("^Settings.*\\.txt")) {
                System.out.println(new Settings(file).isFillAllFields());
            }
        }
        System.out.println("TEST!!!!!!");
    }

    @Test
    public void toArchive() {

        Settings settings1 = new Settings(new File("/Users/ihor/IdeaProjects/testrepo/Settings_Komod.txt"));
        File storagePath = new File(settings1.getStorageBackupPath());
        Arrays.stream(storagePath.listFiles())
                .forEach(System.out::println);
    }

    @Test
    public void testerere() {


        File[] files = new File(Paths.get("").toAbsolutePath().toString().substring(0, 34)).listFiles();



        Arrays.stream(files)
                .peek(o -> System.out.println(o.getAbsolutePath().toString()))
                .filter(o -> o.getName().matches("^Settings.*\\.txt"))
                .filter(file -> new Settings(file).isFillAllFields())
                .map(Settings::new)
                .forEach(k -> {
                    try {
                        Files.walk(Paths.get(k.getFtpPath()))
                                .map(Path::toFile)
                                .forEach(e -> System.out.println(e.getName()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    @Test
    public void mkdirs() {
        String where = "/Users/ihor/Desktop/test/ftp/Region/";

        for (int i = 1; i < 1000; i++) {

            String iterator;

            if (i < 10) {
                iterator = "00";
            } else if (10 <= i & i < 100) {
                iterator = "0";
            } else {
                iterator = "";
            }
            new File(where + "P" + iterator + i + File.separator + "In").mkdirs();
            new File(where + "P" + iterator + i + File.separator + "Out").mkdirs();
        }
    }

    @Test
    public void testyuiop() {
        File file = new File("/Users/ihor/Desktop/test/ftp/Komod/In/In/048170628.002.zip");
        //File file2 = UnzipFile.unzip(file);
        //System.out.println(file2.getName());
    }
}
